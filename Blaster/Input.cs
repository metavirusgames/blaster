﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public sealed class Input {

        private static KeyboardState _previousState;

        public static void Update ( GameTime gameTime ) {
            _previousState = Keyboard.GetState();
        }

        public static bool IsKeyDown ( Keys key ) {
            return Keyboard.GetState().IsKeyDown(key);
        }

        public static bool Checked ( Keys key ) {
            return Keyboard.GetState().IsKeyDown(key) && !_previousState.IsKeyDown(key);
        }
    }
}
