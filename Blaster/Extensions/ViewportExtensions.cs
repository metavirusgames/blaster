﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Xna.Framework {
    public static class ViewportExtensions {

        public static bool InView (this Viewport viewport, Vector2 position ) {

            return ( position.X >= viewport.X && position.X < viewport.X + viewport.Width &&
                position.Y >= viewport.Y && position.Y < viewport.Y + viewport.Height );
        }
    }
}
