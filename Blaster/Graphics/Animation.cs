﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Graphics {
    public class Animation {

        public List<Rectangle> Frames;

        private double _tick = 0f;
        private double _interval = 0f;
        private int _frameIndex = 0;
        private Texture2D _texture;

        public Animation (Texture2D texture, double interval) {

            _texture = texture;

            _interval = interval;

            Frames = new List<Rectangle>();
        }

        public void Update ( GameTime gameTime ) {

            if ( _tick < _interval ) {

                _tick += gameTime.ElapsedGameTime.TotalMilliseconds;
            } else {

                if ( _frameIndex + 1 < Frames.Count ) {
                    _frameIndex += 1;
                } else {
                    _frameIndex = 0;
                }

                _tick = 0;
            }
        }

        public void Draw ( GameTime gameTime, Vector2 position, SpriteBatch spriteBatch, SpriteEffects spriteEffect ) {
            spriteBatch.Draw( _texture, position, Frames[_frameIndex], Color.White, 0f, Vector2.Zero, 1f, spriteEffect, 1f );
        }

        public void AddFrame ( Rectangle rectangle ) {
            Frames.Add( rectangle );
        }
    }
}
