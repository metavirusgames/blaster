﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Graphics {
    public sealed class AnimationFrame {

        private Rectangle _data;

        public AnimationFrame ( int x, int y, int width, int height ) {
            _data = new Rectangle( x, y, width, height );
        }

        public Rectangle GetRectangle () {
            return _data;
        }
    }
}
