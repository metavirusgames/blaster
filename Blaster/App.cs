﻿using Blaster.Entities;
using Blaster.Graphics;
using Blaster.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public sealed class App : Game {

        public ScreenManager ScreenManager { get; private set; }

        public Texture2D Tileset { get; private set; }

        public Texture2D Bullet { get; private set; }

        public Texture2D LevelIcons { get; set; }

        public Texture2D LevelSelectBG { get; private set; }

        public SpriteBatch SpriteBatch { get; private set; }

        public Random Random = new Random( new Random().Next( -10000, 10000 ) );

        public static float Fib = 1.61803398875f;

        public SpriteFont UIFont { get; private set; }

        public SpriteFont StatsFont { get; private set; }

        public Stats Stats { get; private set; }

        private RenderTarget2D _target;
        
        private int _renderTargetWidth;
        private int _renderTargetHeight;

        #region Singleton
        private static object SyncRoot = new object();
        private static App _instance;
        public static App Instance {

            get {
                if ( _instance == null ) {

                    lock ( SyncRoot ) {

                        if ( _instance == null ) {
                            return _instance = new App();
                        }
                    }
                }

                return _instance;
            }
        } 
        #endregion

        public App () {
            _graphics = new GraphicsDeviceManager( this ) { PreferredBackBufferWidth = 800, PreferredBackBufferHeight = 600, IsFullScreen = false };
            ScreenManager = new ScreenManager();
            Stats = new Stats();
        }

        protected override void Initialize () {

            Tileset = Resources.GetTexture( "tiles.png" );
            
            Bullet = Resources.GetTexture( "Bullet.png" );

            LevelIcons = Resources.GetTexture( "LevelIcons.png" );

            LevelSelectBG = Resources.GetTexture( "LevelSelectBG.png" );

            UIFont = Resources.GetSpriteFont( "visitor48.xnb" );

            StatsFont = Resources.GetSpriteFont( "8bit20.xnb" );

            string version = Resources.GetString( "Version" );

            ScreenManager.SetScreen( new LevelSelectionScreen( this ) );

            ScreenManager.Initialize();

            base.Initialize();
        }

        protected override void LoadContent () {

            SpriteBatch = new Microsoft.Xna.Framework.Graphics.SpriteBatch( GraphicsDevice );

            ScreenManager.LoadContent();

            _target = new RenderTarget2D( GraphicsDevice, 800, 600 );

            base.LoadContent();
        }

        protected override void Update ( GameTime gameTime ) {

            //if ( Keyboard.GetState().IsKeyDown( Keys.Escape ) ) Exit();

            ScreenManager.Update( gameTime );

            Input.Update( gameTime );           

            base.Update( gameTime );
        }

        protected override void Draw ( GameTime gameTime ) {

            GraphicsDevice.SetRenderTarget( _target );

            GraphicsDevice.Clear( Color.Transparent );

            SpriteBatch.Begin();

            ScreenManager.Draw( gameTime, SpriteBatch );

            SpriteBatch.End();

            GraphicsDevice.SetRenderTarget( null );

            GraphicsDevice.Clear( Color.Black );

            if ( GraphicsDevice.PresentationParameters.IsFullScreen ) {
                _renderTargetWidth = (int)( this.Window.ClientBounds.Width * 0.70f );
                _renderTargetHeight = (int)( this.Window.ClientBounds.Height * 0.75f );

                Vector2 _renderPosition = new Vector2( this.Window.ClientBounds.Width / 2 - _renderTargetWidth / 2, this.Window.ClientBounds.Height / 2 - _renderTargetHeight / 2 );

                SpriteBatch.Begin();

                SpriteBatch.Draw( _target, new Rectangle( (int)_renderPosition.X, (int)_renderPosition.Y, _renderTargetWidth, _renderTargetHeight ), Color.White );

                SpriteBatch.End();
            } else {
                SpriteBatch.Begin();

                SpriteBatch.Draw( _target, new Rectangle(0, 0, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height ), Color.White );

                SpriteBatch.End();
            }

            base.Draw( gameTime );
        }

        protected override void Dispose ( bool disposing ) {

            ScreenManager.Dispose();
            
            base.Dispose( disposing );
        }

        private GraphicsDeviceManager _graphics;


    }
}
