﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blaster.Core {
    public class MathUtil {

        public static int GetFibScale ( int value ) {
            return (int)( 10 * ( Math.Pow( 2, value ) ) );
        }

        public static int GetBasicScale ( int coefficient, int value ) {
            return (int)( coefficient * value * App.Fib );
        }
    }
}
