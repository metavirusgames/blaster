﻿using Blaster.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Blaster {
    public class Stats {

        public Dictionary<int, int> WavesSurvived { get; private set; }

        public int Highscore { get; private set; }

        private Dictionary<int, int> _waveUnlock;

        public Stats () {

            _waveUnlock = new Dictionary<int, int>();

            _waveUnlock.Add( 0, 0 );
            _waveUnlock.Add( 1, 1 );
            _waveUnlock.Add( 2, 2 );
            _waveUnlock.Add( 3, 4 );
            _waveUnlock.Add( 4, 6 );
            _waveUnlock.Add( 5, 7 );

            _waveUnlock.Add( 6, 8 );
            _waveUnlock.Add( 7, 10 );
            _waveUnlock.Add( 8, 11 );
            _waveUnlock.Add( 9, 13 );
            _waveUnlock.Add( 10, 14 );

            _waveUnlock.Add( 11, 15 );
            _waveUnlock.Add( 12, 17 );
            _waveUnlock.Add( 13, 18 );
            _waveUnlock.Add( 14, 20 );
            _waveUnlock.Add( 15, 21 );

            _waveUnlock.Add( 16, 22 );
            _waveUnlock.Add( 17, 23 );
            _waveUnlock.Add( 18, 25 );
            _waveUnlock.Add( 19, 27 );

            if ( !File.Exists( "Game.sav" ) ) {
                WavesSurvived = new Dictionary<int, int>();

                

                SetWavesSurvived( 0, 0 );
            } else {
                Load();
            }
        }

        public void SetWavesSurvived ( int level, int value ) {

            if ( WavesSurvived.ContainsKey( level ) ) {

                //We only want to accept new values if they are higher
                if ( WavesSurvived[level] < value ) {
                    WavesSurvived[level] = value;
                }
            } else {
                WavesSurvived[level] = value;
            }

            Save();
        }

        public void SetScore ( int value ) {

            //We only want to accept a new high score if the new value is higher
            if ( Highscore < value ) {
                Highscore = value;
            }
        }

        public bool IsLevelUnlocked ( int level ) {
            level = level - 1;

            if ( WavesSurvived.ContainsKey( level ) ) {

                if ( WavesSurvived[level] >= _waveUnlock[level] ) {
                    return true;
                }
            }

            return false;
        }

        public int GetUnlockRequirement ( int level ) {
            if ( _waveUnlock.ContainsKey( level ) ) {
                return _waveUnlock[level];
            } else {
                throw new Exception( level + " was not found in the unlock list");
            }
        }

        public void Save () {
            FileStream stream = new FileStream( "Game.sav", FileMode.Create );
            BinaryWriter writer = new BinaryWriter( stream );

            writer.Write( WavesSurvived.Count );

            foreach ( var kvp in WavesSurvived ) {
                writer.Write( String.Format("{0},{1}", kvp.Key, kvp.Value) );
            }

            writer.Close();
            stream.Close();
        }

        public void Load () {

            if ( File.Exists( "Game.sav" ) ) {
                FileStream stream = new FileStream( "Game.sav", FileMode.Open, FileAccess.Read );
                BinaryReader reader = new BinaryReader( stream );

                List<string> _info = new List<string>();

                int total = reader.ReadInt32();

                for ( int i = 0; i < total; i++ ) {
                    string line = reader.ReadString();
                    _info.Add( line );
                }

                WavesSurvived = new Dictionary<int, int>();

                foreach ( string line in _info ) {
                    string[] data = line.Split( ',' );
                    int level = int.Parse( data[0] );
                    int stat = int.Parse( data[1] );

                    WavesSurvived.Add( level, stat );
                }

                reader.Close();
                stream.Close();
            }

            
        }
    }
}
