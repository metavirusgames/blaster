﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public sealed class Program {

        public static void Main ( String[] args ) {

            App.Instance.Run();
        }
    }
}
