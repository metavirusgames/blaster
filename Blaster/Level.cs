﻿using Blaster.Entities;
using Blaster.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public sealed class Level : IDisposable {

        public const int LEVEL_SIZE = 475;

        public const int LEVEL_WIDTH = 25;

        public const int LEVEL_HEIGHT = 19;

        private int[,] _data;

        public List<Rectangle> LevelBlocks { get; private set; }

        private Texture2D _texture;

        private Texture2D _bg;

        public int WaveCount = 0;
        public int WaveMax = 10;
        public int WaveRemaining = 10;

        public bool WaitingForNextWave = false;

        public List<IEntity> Entities;

        public int _blockIndex = 0;

        public delegate void ScoreEventHandler ( object sender, EventArgs e );

        public delegate void EntityDeathEventHandler ( object sender, EventArgs e );

        public delegate void WaveBeginEventHandler ( object sender, EventArgs e );

        public delegate void WaveEndEventHandler ( object sender, EventArgs e );

        public event ScoreEventHandler Scored;

        public event EntityDeathEventHandler EntityDeath;

        public event WaveBeginEventHandler WaveStarted;

        public event WaveEndEventHandler WaveEnded;

        private double _tick = 0D;
        private double _interval = 2500D;
        private SoundEffect _explodeSound;
        private SoundEffect _pickupSound;

        private double _nextWaveTick = 0D;
        private double _nextWaveInterval = 1000D;
        private int _nextWaveWait;
        private int _nextWaveWaitTick;
        private bool _waveEnded = false;

        public Level ( Texture2D texture ) {
            _texture = texture;

            int bgRandom = App.Instance.Random.Next( 1, 3 );

            _bg = Resources.GetTexture( "BG" + bgRandom + ".png" );
            _explodeSound = Resources.GetSoundEffect( "Explosion.wav" );
            _pickupSound = Resources.GetSoundEffect( "Pickup.wav" );
            InitializeLevel();
        }

        private float lastMovement = 1.0f;

        public void Update ( GameTime gameTime ) {

            List<IEntity> entitiesToRemove = new List<IEntity>();

            if ( !WaitingForNextWave && WaveRemaining > 0 ) {
                if ( _tick > _interval ) {

                    int spawn = App.Instance.Random.Next( 100 );

                    CharacterEntity unit = null;

                    if ( spawn < 25 ) {
                        unit = new Probe( this, new Vector2( 12 * 32, 0 ) );
                    } else {
                        int unitChance = App.Instance.Random.Next( 100 );
                        if ( unitChance < 15 ) {
                            unit = new X5Unit( this, new Vector2( 12 * 32, 0 ) );
                        } else {
                            unit = new X3Unit( this, new Vector2( 12 * 32, 0 ) );
                        }
                    }

                    if ( lastMovement == 1f ) {
                        lastMovement = -1f;
                    } else {
                        lastMovement = 1f;
                    }

                    unit.Initialize();
                    unit.LoadContent();
                    unit.SetMovement( lastMovement );
                    Entities.Add( unit );
                    WaveRemaining -= 1;
                    _tick = 0D;
                } else {
                    _tick += gameTime.ElapsedGameTime.TotalMilliseconds;
                }
            }

            if ( WaitingForNextWave == false && ( WaveMax - WaveCount ) <= 0 ) {
                OnWaveEnded();
                WaitingForNextWave = true;
            }

            Player player = (Player)Entities.FirstOrDefault( e => e is Player );

            foreach ( IEntity entity in Entities ) {
                entity.Update( gameTime );

                if ( ( entity is CharacterEntity ) ) {
                    if ( !App.Instance.GraphicsDevice.Viewport.InView( ( entity as CharacterEntity ).Position ) && ( entity as CharacterEntity ).IsAlive ) {

                        if ( !( entity is SawBlade ) ) {
                            ( entity as CharacterEntity ).SetPosition( new Vector2( 12 * 32, 0 ) );
                        } else {
                            entitiesToRemove.Add( entity );
                        }
                    } else if ( !( entity as CharacterEntity ).IsAlive ) {
                        entitiesToRemove.Add( entity );
                        WaveCount += 1;
                    } else {
                        if ( player != null && !( entity is Player ) && ( entity as CharacterEntity ).BoundingRectangle.Intersects( player.BoundingRectangle ) ) {
                            //player.Kill();
                        }
                    }
                } else if ( ( entity is Gun ) && player.BoundingRectangle.Intersects( new Rectangle( (int)( entity as Gun ).Position.X, (int)( entity as Gun ).Position.Y, 32, 32 ) ) ) {
                    player.GunLevel = ( entity as Gun ).Level;

                    if ( player.GunLevel == 1 ) {
                        player.Ammo = 40;
                    }

                    if ( player.GunLevel > 1 ) {
                        player.Ammo = 80;
                    }

                    entitiesToRemove.Add( entity );
                    _pickupSound.Play();
                }
            }



            foreach ( IEntity deadEntity in entitiesToRemove ) {

                if ( deadEntity is CharacterEntity ) {
                    _explodeSound.Play();

                    if ( deadEntity is X5Unit ) {
                        SawBlade sawBlade = new SawBlade( this, ( deadEntity as CharacterEntity ).Position );
                        sawBlade.Initialize();
                        sawBlade.LoadContent();
                        sawBlade.SetMovement( ( deadEntity as CharacterEntity ).Movement );
                        Entities.Add( sawBlade );
                    }
                }

                int chanceToSpawnGun = App.Instance.Random.Next( 100 );

                if ( chanceToSpawnGun < 40 ) {
                    if ( deadEntity is CharacterEntity ) {

                        if ( player != null ) {
                            int gunLevel = -1;
                            if ( player.GunLevel == 0 ) {
                                gunLevel = 1;
                                //player.Ammo = 40;
                            } else if ( player.GunLevel == 1 ) {
                                gunLevel = 2;
                                //player.Ammo = 80;
                            }

                            if ( gunLevel < 3 && gunLevel > -1 ) {
                                Entities.Add( new Gun( this, ( deadEntity as CharacterEntity ).Position, gunLevel ) );
                            }
                        }
                    }
                }

                Entities.Remove( deadEntity );
            }
        }

        public void Draw ( GameTime gameTime, SpriteBatch spriteBatch ) {

            spriteBatch.Draw( _bg, Vector2.Zero, Microsoft.Xna.Framework.Color.White );

            for ( int y = 0; y < LEVEL_HEIGHT; y++ ) {
                for ( int x = 0; x < LEVEL_WIDTH; x++ ) {
                    int cellValue = _data[x, y];

                    if ( cellValue > 0 ) {
                        spriteBatch.Draw( App.Instance.Tileset, new Vector2( x * 32, y * 32 ), LevelBlocks[_blockIndex], Microsoft.Xna.Framework.Color.White );
                    }
                }
            }

            foreach ( IEntity entity in Entities ) {
                entity.Draw( gameTime, spriteBatch );
            }
        }

        public TileCollision GetCollision ( int x, int y ) {
            int location = x * y;

            if ( isInBounds( x, y ) ) {
                int tileValue = _data[x, y];

                if ( tileValue == 0 ) return TileCollision.None;

                if ( tileValue == 1 ) return TileCollision.Impassable;
            }

            return TileCollision.None;
        }

        public Microsoft.Xna.Framework.Rectangle GetBounds ( int x, int y ) {

            if ( isInBounds( x, y ) ) {
                return new Microsoft.Xna.Framework.Rectangle( x * 32, y * 32, 32, 32 );
            }

            return new Microsoft.Xna.Framework.Rectangle();
        }

        private bool isInBounds ( int x, int y ) {
            int location = x * y;

            if ( x >= 0 && x < LEVEL_WIDTH && y >= 0 && y < LEVEL_HEIGHT ) {
                return true;
            }

            return false;
        }

        public void InitializeLevel () {

            Entities = new List<IEntity>();

            LevelBlocks = new List<Rectangle>();

            Microsoft.Xna.Framework.Color[] colorData = new Microsoft.Xna.Framework.Color[_texture.Width * _texture.Height];
            _texture.GetData<Microsoft.Xna.Framework.Color>( colorData );
            _data = new int[LEVEL_WIDTH, LEVEL_HEIGHT];

            if ( colorData != null && colorData.Length == LEVEL_SIZE ) {

                for ( int y = 0; y < LEVEL_HEIGHT; y++ ) {
                    for ( int x = 0; x < LEVEL_WIDTH; x++ ) {
                        Microsoft.Xna.Framework.Color color = colorData[y * LEVEL_WIDTH + x];

                        if ( color == Microsoft.Xna.Framework.Color.Black ) {
                            _data[x, y] = 1;
                        } else {
                            _data[x, y] = 0;
                        }
                    }
                }

            }

            LevelBlocks.Add( new Rectangle( 32, 0, 32, 32 ) );
            LevelBlocks.Add( new Rectangle( 64, 0, 32, 32 ) );
            LevelBlocks.Add( new Rectangle( 96, 0, 32, 32 ) );
            LevelBlocks.Add( new Rectangle( 128, 0, 32, 32 ) );
            LevelBlocks.Add( new Rectangle( 160, 0, 32, 32 ) );

            _blockIndex = App.Instance.Random.Next( 0, LevelBlocks.Count );
        }

        public void Dispose () {
            _data = null;
            _texture = null;
        }

        #region Events

        public void OnScore ( ScoreEventArgs e ) {
            if ( Scored != null ) {
                Scored( this, e );
            }
        }

        public void OnEntityDeath ( EntityDeathEventArgs e ) {
            if ( EntityDeath != null ) {
                EntityDeath( this, e );
            }
        }

        public void OnWaveStarted () {
            if ( WaveStarted != null ) {
                WaveStarted( this, null );
            }
        }

        public void OnWaveEnded () {
            if ( WaveEnded != null ) {
                WaveEnded( this, null );
            }
        }
        #endregion
    }
}
