﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Entities {
    public interface IEntity {

        Level Parent { get; }

        void Update ( GameTime gameTime );

        void Draw ( GameTime gameTime, SpriteBatch spriteBatch );
    }
}
