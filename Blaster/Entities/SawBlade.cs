﻿using Blaster.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Entities {
    public class SawBlade  : CharacterEntity {

        private Rectangle _sawBlade = new Rectangle( 0, 64, 32, 32 );
        private Texture2D _texture;

        public SawBlade ( Level parent, Vector2 position )
            : base( parent ) {
            Position = position;
        }

        public override void Initialize () {
            MoveAcceleration = 19000f;
            MaxMoveSpeed = 4000f;
        }

        public override void LoadContent () {

            int width = (int)( 32 * 0.6 );
            int left = ( 32 - width ) / 2;
            int height = (int)( 32 * 0.8 );
            int top = 32 - height;
            localBounds = new Rectangle( left, top, width, height );

            //SetAnimation( WalkAnimation );

            Health = 999;

            WorthPoints = 10;

            _texture = Resources.GetTexture( "X5Animation.png" );
        }

        private KeyboardState _previousKeyboard;

        public override void Update ( GameTime gameTime ) {
            KeyboardState keyboard = Keyboard.GetState();

            ApplyPhysics( gameTime );

            IsJumping = false;

            _previousKeyboard = keyboard;
            base.Update( gameTime );
        }

        private float rotation = 0f;

        public override void Draw ( GameTime gameTime, SpriteBatch spriteBatch ) {

            rotation += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if ( Heading == Entities.Heading.Left ) {
                spriteBatch.Draw( _texture, Position - new Vector2( 4, -16 ), _sawBlade, Color.White, rotation, new Vector2( 16, 16 ), 1f, SpriteEffects.FlipHorizontally, 1f );
            } else {
                spriteBatch.Draw( _texture, Position + new Vector2( 32, 16 ), _sawBlade, Color.White, rotation, new Vector2( 16, 16 ), 1f, SpriteEffects.FlipHorizontally, 1f );
            }

            //base.Draw( gameTime, spriteBatch );
        }

        public override void Collide ( Axis axis ) {

            if ( Movement == 1f ) {
                Movement = -1f;
                Heading = Entities.Heading.Left;
            } else {
                Movement = 1f;
                Heading = Entities.Heading.Right;
            }

            base.Collide( axis );
        }
    }
}
