﻿using Blaster.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Entities {
    class Probe   : CharacterEntity {

        private Vector2 basePosition;
        private float bounce;

        //public override Vector2 Position {
        //    get {
        //        return base.Position + new Vector2(0.0f, bounce);
        //    }
        //    protected set {
        //        base.Position = value;
        //    }
        //}

        public Probe ( Level parent, Vector2 position )
            : base( parent )
        {
            Position = position;
            basePosition = Position;
        }

        public override void Initialize () {
            MoveAcceleration = 17000f;
            MaxMoveSpeed = 2500f;
        }

        public override void LoadContent () {

            IdleAnimation = new Animation( Resources.GetTexture( "Probe.png" ), 100.0D );
            IdleAnimation.AddFrame( new Rectangle( 0, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 32, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 64, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 96, 0, 32, 32 ) );

            WalkAnimation = IdleAnimation;
            AttackAnimation = IdleAnimation;

            //WalkAnimation = new Animation( Resources.GetTexture( "Probe.png" ), 100.0D );
            //WalkAnimation.AddFrame( new Rectangle( 0, 32, 32, 32 ) );
            //WalkAnimation.AddFrame( new Rectangle( 32, 32, 32, 32 ) );
            //WalkAnimation.AddFrame( new Rectangle( 64, 32, 32, 32 ) );
            //WalkAnimation.AddFrame( new Rectangle( 96, 32, 32, 32 ) );

            //AttackAnimation = new Animation( Resources.GetTexture( "Probe.png" ), 100.0D );
            //AttackAnimation.AddFrame( new Rectangle( 0, 32, 32, 32 ) );
            //AttackAnimation.AddFrame( new Rectangle( 32, 32, 32, 32 ) );
            //AttackAnimation.AddFrame( new Rectangle( 64, 32, 32, 32 ) );
            //AttackAnimation.AddFrame( new Rectangle( 96, 32, 32, 32 ) );

            int width = (int)( 32 * 0.6 );
            int left = ( 32 - width ) / 2;
            int height = (int)( 32 * 0.8 );
            int top = 32 - height;
            localBounds = new Rectangle( left, top, width, height );

            SetAnimation( WalkAnimation );

            Health = 2;

            WorthPoints = 5;
        }

        private KeyboardState _previousKeyboard;

        public override void Update ( GameTime gameTime ) {

            const float BounceHeight = 0.18f;
            const float BounceRate = 3.0f;
            const float BounceSync = -0.75f;

            double t = gameTime.TotalGameTime.TotalSeconds * BounceRate + Position.X * BounceSync;
            bounce = (float)Math.Sin( t ) * BounceHeight * 32;

            Player player = (Player)Parent.Entities.FirstOrDefault(e => e is Player);

            if (player != null){
                Vector2 velocityToPlayer = ( player.Position - Position );
                velocityToPlayer.Normalize();
                velocityToPlayer = velocityToPlayer * new Vector2(1, App.Fib);
                Position += velocityToPlayer;
            }
            base.Update( gameTime );
        }

        public override void Collide ( Axis axis ) {

            if ( Movement == 1f ) {
                Movement = -1f;
                Heading = Entities.Heading.Left;
            } else {
                Movement = 1f;
                Heading = Entities.Heading.Right;
            }

            base.Collide( axis );
        }
    }
}
