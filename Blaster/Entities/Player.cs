﻿using Blaster.Events;
using Blaster.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Entities {
    public sealed class Player : CharacterEntity {

        private List<Bullet> _bullets;
        private double _tick = 0D;
        private double _interval = 150D;
        private bool _canFire = true;
        private SoundEffect _gunSound;
        private SoundEffect _machineGunSound;
        private SoundEffect _explodeSound;
        public int GunLevel;
        public int Ammo;

        public Player ( Level parent, Vector2 position ) : base( parent )
        {
            Position = position;

            _bullets = new List<Bullet>();
        }

        public override void Initialize () {
            //throw new NotImplementedException();
        }

        public override void LoadContent () {

            IdleAnimation = new Animation( Resources.GetTexture( "X4Animation.png" ), 100.0D );
            IdleAnimation.AddFrame( new Rectangle( 0, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 32, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 64, 0, 32, 32 ) );
            IdleAnimation.AddFrame( new Rectangle( 96, 0, 32, 32 ) );

            WalkAnimation = new Animation( Resources.GetTexture( "X4Animation.png" ), 100.0D );
            WalkAnimation.AddFrame( new Rectangle( 0, 32, 32, 32 ) );
            WalkAnimation.AddFrame( new Rectangle( 32, 32, 32, 32 ) );
            WalkAnimation.AddFrame( new Rectangle( 64, 32, 32, 32 ) );
            WalkAnimation.AddFrame( new Rectangle( 96, 32, 32, 32 ) );

            AttackAnimation = new Animation( Resources.GetTexture( "X4Animation.png" ), 100.0D );
            AttackAnimation.AddFrame( new Rectangle( 0, 32, 32, 32 ) );
            AttackAnimation.AddFrame( new Rectangle( 32, 32, 32, 32 ) );
            AttackAnimation.AddFrame( new Rectangle( 64, 32, 32, 32 ) );
            AttackAnimation.AddFrame( new Rectangle( 96, 32, 32, 32 ) );

            _gunSound = Resources.GetSoundEffect( "Gun.wav" );
            _machineGunSound = Resources.GetSoundEffect( "MachineGun.wav" );

            int width = (int)( 32 * 0.6 );
            int left = ( 32 - width ) / 2;
            int height = (int)( 32 * 0.8 );
            int top = 32 - height;
            localBounds = new Rectangle( left, top, width, height );
        }

        private KeyboardState _previousKeyboard;

        public override void Update ( GameTime gameTime ) {
            KeyboardState keyboard = Keyboard.GetState();

            if ( Input.IsKeyDown( Keys.Left ) ) {
                Heading = Entities.Heading.Left;
                SetAnimation( WalkAnimation );
                Movement = -1f;
            } else if ( Input.IsKeyDown( Keys.Right ) ) {
                Heading = Entities.Heading.Right;
                SetAnimation( WalkAnimation );
                Movement = 1f;
            }else {
                SetAnimation( IdleAnimation );
            }

            if ( keyboard.IsKeyDown( Keys.Z ) && !_previousKeyboard.IsKeyDown( Keys.Z ) || keyboard.IsKeyDown( Keys.Up ) && !_previousKeyboard.IsKeyDown( Keys.Up ) ) {
                IsJumping = true;
            }

            if ( _tick > _interval && !_canFire) {
                _canFire = true;
                _tick = 0D;
            } else {
                _tick += gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            if ( GunLevel == 0 ) {
                _interval = 150D;
                if ( keyboard.IsKeyDown( Keys.X ) && !_previousKeyboard.IsKeyDown( Keys.X ) && _canFire ) {
                    Vector2 velocity = Heading == Entities.Heading.Left ? new Vector2( -8, 0 ) : new Vector2( 8, 0 );

                    _bullets.Add( new Bullet() { Position = this.Position + new Vector2( 16, 16 ), Velocity = velocity } );
                    _canFire = false;
                    _gunSound.Play();
                }
            } else if ( GunLevel == 1 ) {
                _interval = 75D;
                if ( keyboard.IsKeyDown( Keys.X ) && _canFire ) {
                    Vector2 velocity = Heading == Entities.Heading.Left ? new Vector2( -8, 0 ) : new Vector2( 8, 0 );

                    _bullets.Add( new Bullet() { Position = this.Position + new Vector2( 16, 16 ), Velocity = velocity } );
                    Velocity += velocity * new Vector2( -50, 0 );
                    _canFire = false;
                    _machineGunSound.Play();

                    if ( Ammo - 1 <= 0 ) {
                        GunLevel = 0;
                        Ammo = 0;
                    } else {
                        Ammo -= 1;
                    }
                }
            } else if ( GunLevel > 1 ) {
                _interval = 50D;
                if ( keyboard.IsKeyDown( Keys.X ) && _canFire ) {
                    Vector2 velocity = Heading == Entities.Heading.Left ? new Vector2( -8, 0 ) : new Vector2( 8, 0 );
                    Velocity += velocity * new Vector2( -100, 0 );
                    _bullets.Add( new Bullet() { Position = this.Position + new Vector2( 16, 16 ), Velocity = velocity } );
                    _canFire = false;
                    _machineGunSound.Play();

                    if ( Ammo - 1 <= 0 ) {
                        GunLevel = 0;
                        Ammo = 0;
                    } else {
                        Ammo -= 1;
                    }
                }
            }

            List<Bullet> _removeList = new List<Bullet>();

            foreach ( Bullet bullet in _bullets ) {

                if ( App.Instance.GraphicsDevice.Viewport.InView( bullet.Position ) && Parent.GetCollision((int)(bullet.Position.X / 32), (int)(bullet.Position.Y / 32) ) == TileCollision.None) {
                    bullet.Position += bullet.Velocity;
                } else {
                    _removeList.Add( bullet );
                }

                foreach ( IEntity entity in Parent.Entities ) {
                    if ( entity is CharacterEntity ) {
                        if ( !(entity is Player) && ( entity as CharacterEntity ).BoundingRectangle.Intersects( new Rectangle((int)bullet.Position.X, (int)bullet.Position.Y, 4, 4) ) ) {

                            if ( !( entity is SawBlade ) ) {
                                ( entity as CharacterEntity ).Damage( bullet.Velocity );

                                if ( !( entity as CharacterEntity ).IsAlive ) {
                                    Parent.OnScore( new ScoreEventArgs( ( entity as CharacterEntity ).WorthPoints ) );
                                }
                            }
                            _removeList.Add( bullet );
                        }
                    }
                }
            }

            foreach ( Bullet bullet in _removeList ) {

                _bullets.Remove( bullet );
            }

            ApplyPhysics( gameTime );

            Movement = 0f;

            IsJumping = false;

            _previousKeyboard = keyboard;
            base.Update( gameTime );
        }

        public override void Draw ( GameTime gameTime, SpriteBatch spriteBatch ) {

            foreach ( Bullet bullet in _bullets ) {
                spriteBatch.Draw( App.Instance.Bullet, bullet.Position, Color.White );
            }

            base.Draw( gameTime, spriteBatch );
        }
    }
}
