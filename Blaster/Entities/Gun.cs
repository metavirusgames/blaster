﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blaster.Entities {
    public class Gun : IEntity {

        public Level Parent { get; private set; }

        private Vector2 basePosition;
        private float bounce;
        private Texture2D _texture;
        public int Level;

        public Vector2 Position {
            get {
                return basePosition + new Vector2( 0.0f, bounce );
            }
        }

        public Gun ( Level parent, Vector2 position, int level ) {
            Parent = parent;
            basePosition = position;

            _texture = Resources.GetTexture( "Guns.png" );
            Level = level;

        }

        public void Update ( Microsoft.Xna.Framework.GameTime gameTime ) {
            // Bounce control constants
            const float BounceHeight = 0.18f;
            const float BounceRate = 3.0f;
            const float BounceSync = -0.75f;

            // Bounce along a sine curve over time.
            // Include the X coordinate so that neighboring gems bounce in a nice wave pattern.            
            double t = gameTime.TotalGameTime.TotalSeconds * BounceRate + Position.X * BounceSync;
            bounce = (float)Math.Sin( t ) * BounceHeight * 32;
        }

        public void Draw ( Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch ) {
            Rectangle gunRect = new Rectangle();

            if ( Level == 1 ) {
                gunRect = new Rectangle(0, 0, 32, 32);
            } else if ( Level == 2 ) {
                gunRect = new Rectangle( 32, 0, 32, 32 );
            }

            spriteBatch.Draw( _texture, Position, gunRect, Color.White );
        }
    }
}
