﻿using Blaster.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Entities {
    public abstract class CharacterEntity : IEntity {

        public Animation IdleAnimation { get; protected set; }

        public Animation WalkAnimation { get; protected set; }

        public Animation AttackAnimation { get; protected set; }

        public virtual Vector2 Position { get; protected set; }

        private Animation _currentAnimation;

        public Level Parent { get; private set; }

        public Heading Heading { get; protected set; }

        public bool IsAlive { get; protected set; }

        public int Health { get; protected set; }

        public int WorthPoints { get; protected set; }

        // Constants for controling horizontal movement
        protected float MoveAcceleration = 13000.0f;
        protected float MaxMoveSpeed = 1750.0f;
        protected float GroundDragFactor = 0.48f;
        protected float AirDragFactor = 0.48f;

        // Constants for controlling vertical movement
        protected float MaxJumpTime = 0.45f;
        protected float JumpLaunchVelocity = -3500.0f;
        protected float GravityAcceleration = 3400.0f;
        protected float MaxFallSpeed = 550.0f;
        protected float JumpControlPower = 0.14f;

        // Input configuration
        private const float MoveStickScale = 1.0f;
        private const float AccelerometerScale = 1.5f;

        /// <summary>
        /// Current user movement input.
        /// </summary>
        public float Movement;

        // Jumping state
        protected bool IsJumping;
        private bool wasJumping;
        private float jumpTime;

        protected Vector2 Velocity;

        protected Rectangle localBounds;
        /// <summary>
        /// Gets a rectangle which bounds this player in world space.
        /// </summary>
        public Rectangle BoundingRectangle {
            get {
                int left = (int)Math.Round( Position.X ) + localBounds.X;
                int top = (int)Math.Round( Position.Y ) + localBounds.Y;

                return new Rectangle( left, top, localBounds.Width, localBounds.Height );
            }
        }

        private float previousBottom;

        public bool IsOnGround {
            get { return isOnGround; }
        }
        bool isOnGround;

        public CharacterEntity ( Level level ) {
            Parent = level;
            IsAlive = true;
        }

        public abstract void Initialize ();

        public abstract void LoadContent ();

        public virtual void Update ( Microsoft.Xna.Framework.GameTime gameTime ) {

            if ( _currentAnimation != null ) {
                _currentAnimation.Update( gameTime );
            }
        }

        public virtual void Draw ( Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch ) {

            SpriteEffects effect = Heading == Entities.Heading.Left ? SpriteEffects.FlipHorizontally : SpriteEffects.None;

            if ( _currentAnimation != null ) {
                _currentAnimation.Draw( gameTime, Position, spriteBatch, effect );
            }
        }

        public void ApplyPhysics ( GameTime gameTime ) {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 previousPosition = Position;

            // Base velocity is a combination of horizontal movement control and
            // acceleration downward due to gravity.
            Velocity.X += Movement * MoveAcceleration * elapsed;
            Velocity.Y = MathHelper.Clamp( Velocity.Y + GravityAcceleration * elapsed, -MaxFallSpeed, MaxFallSpeed );

            Velocity.Y = DoJump( Velocity.Y, gameTime );

            // Apply pseudo-drag horizontally.
            if ( IsOnGround )
                Velocity.X *= GroundDragFactor;
            else
                Velocity.X *= AirDragFactor;

            // Prevent the player from running faster than his top speed.            
            Velocity.X = MathHelper.Clamp( Velocity.X, -MaxMoveSpeed, MaxMoveSpeed );

            // Apply velocity.
            Position += Velocity * elapsed;
            Position = new Vector2( (float)Math.Round( Position.X ), (float)Math.Round( Position.Y ) );

            // If the player is now colliding with the level, separate them.
            HandleCollisions();

            // If the collision stopped us from moving, reset the velocity to zero.
            if ( Position.X == previousPosition.X )
                Velocity.X = 0;

            if ( Position.Y == previousPosition.Y )
                Velocity.Y = 0;
        }

        private float DoJump ( float velocityY, GameTime gameTime ) {
            // If the player wants to jump
            if ( IsJumping ) {
                // Begin or continue a jump
                if ( ( !wasJumping && IsOnGround ) || jumpTime > 0.0f ) {
                    if ( jumpTime == 0.0f )
                        //jumpSound.Play();

                        jumpTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    SetAnimation( WalkAnimation );
                }

                // If we are in the ascent of the jump
                if ( 0.0f < jumpTime && jumpTime <= MaxJumpTime ) {
                    // Fully override the vertical velocity with a power curve that gives players more control over the top of the jump
                    velocityY = JumpLaunchVelocity * ( 1.0f - (float)Math.Pow( jumpTime / MaxJumpTime, JumpControlPower ) );
                } else {
                    // Reached the apex of the jump
                    jumpTime = 0.0f;
                }
            } else {
                // Continues not jumping or cancels a jump in progress
                jumpTime = 0.0f;
            }
            wasJumping = IsJumping;

            return velocityY;
        }

        public void HandleCollisions () {
            // Get the player's bounding rectangle and find neighboring tiles.
            Rectangle bounds = BoundingRectangle;
            int leftTile = (int)Math.Floor( (float)bounds.Left / 32 );
            int rightTile = (int)Math.Ceiling( ( (float)bounds.Right / 32 ) ) - 1;
            int topTile = (int)Math.Floor( (float)bounds.Top / 32 );
            int bottomTile = (int)Math.Ceiling( ( (float)bounds.Bottom / 32 ) ) - 1;

            // Reset flag to search for ground collision.
            isOnGround = false;

            // For each potentially colliding tile,
            for ( int y = topTile; y <= bottomTile; ++y ) {
                for ( int x = leftTile; x <= rightTile; ++x ) {
                    // If this tile is collidable,
                    TileCollision collision = Parent.GetCollision( x, y );
                    if ( collision != TileCollision.None ) {
                        // Determine collision depth (with direction) and magnitude.
                        Rectangle tileBounds = Parent.GetBounds( x, y );
                        Vector2 depth = RectangleExtensions.GetIntersectionDepth( bounds, tileBounds );
                        if ( depth != Vector2.Zero ) {
                            float absDepthX = Math.Abs( depth.X );
                            float absDepthY = Math.Abs( depth.Y );

                            // Resolve the collision along the shallow axis.
                            //if ( absDepthY < absDepthX || collision == TileCollision.Platform ) {
                            if ( absDepthY < absDepthX ) {
                                // If we crossed the top of a tile, we are on the ground.
                                if ( previousBottom <= tileBounds.Top )
                                    isOnGround = true;

                                // Ignore platforms, unless we are on the ground.
                                if ( collision == TileCollision.Impassable || IsOnGround ) {
                                    // Resolve the collision along the Y axis.
                                    Position = new Vector2( Position.X, Position.Y + depth.Y );

                                    // Perform further collisions with the new bounds.
                                    bounds = BoundingRectangle;
                                }
                            } else if ( collision == TileCollision.Impassable ) // Ignore platforms.
                            {
                                // Resolve the collision along the X axis.
                                Position = new Vector2( Position.X + depth.X, Position.Y );

                                Vector2 actualPosition = new Vector2( (int)( Position.X / 32 ), (int)( Position.Y / 32 ) );
                                TileCollision nextCollision = TileCollision.None;


                                if ( Movement == 1f ) {
                                    nextCollision = Parent.GetCollision( rightTile, bottomTile -1 );
                                } else {
                                    nextCollision = Parent.GetCollision( leftTile, bottomTile -1 );
                                }

                                if ( nextCollision == TileCollision.Impassable ) {
                                    Collide( Axis.Horizontal );
                                }


                                // Perform further collisions with the new bounds.
                                bounds = BoundingRectangle;
                            }
                        }
                    }
                }
            }

            // Save the new bounds bottom.
            previousBottom = bounds.Bottom;
        }

        public void SetAnimation ( Animation animation ) {
            _currentAnimation = animation;
        }

        public void SetMovement ( float movement ) {
            Movement = movement;

            if ( movement == 1f ) {
                Heading = Entities.Heading.Right;
            } else {
                Heading = Entities.Heading.Left;
            }
        }

        public void Damage ( Vector2 direction ) {

            if ( direction.X < 0 ) {
                Velocity.X = -1000;
            } else {
                Velocity.X = 1000;
            }

            Damage();
        }

        public void Damage () {
            Health -= 1;

            if ( Health <= 0 ) {
                Kill();
            }
        }

        public void Kill () {
            IsAlive = false;

            Parent.OnEntityDeath( new Events.EntityDeathEventArgs( this ) );
        }

        public enum Axis {
            Vertical,
            Horizontal
        }

        public void SetPosition ( Vector2 position ) {
            Position = position;
        }

        public void SetAlive () {
            IsAlive = true;
        }

        public virtual void Collide ( Axis axis ) {

        }
    }
}
