﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public enum TileCollision {

        None,
        Impassable,
        Spring,
        Effect
    }
}
