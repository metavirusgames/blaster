﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Screens {
    public sealed class ScreenManager : IDisposable {

        private List<Screen> _screens;
        private List<Screen> _addQueue;

        public ScreenManager () {
            _screens = new List<Screen>();
            _addQueue = new List<Screen>();
        }

        public void Initialize () {
            foreach ( Screen screen in _screens ) {
                screen.Initialize();
            }
        }

        public void LoadContent () {
            foreach ( Screen screen in _screens ) {
                screen.LoadContent();
            }
        }

        public void Update ( GameTime gameTime ) {
            foreach ( Screen screen in _addQueue ) {
                _screens.Add( screen );
            }

            _addQueue.Clear();

            foreach ( Screen screen in _screens ) {

                if ( screen.IsActive ) {
                    screen.Update( gameTime );
                }
            }
        }

        public void Draw ( GameTime gameTime, SpriteBatch spriteBatch ) {

            foreach ( Screen screen in _screens ) {
                if ( screen.IsActive ) {
                    screen.Draw( gameTime, spriteBatch );
                }
            }
        }

        public void SetScreen ( Screen screen ) {

            foreach ( Screen item in _screens ) {
                item.IsActive = false;
            }

            if ( !_screens.Contains( screen ) ) {
                if ( screen.ContentLoaded == false ) {
                    screen.Initialize();
                    screen.LoadContent();
                }

                screen.IsActive = true;
                _addQueue.Add( screen );

            } else {
                _screens[_screens.IndexOf( screen )].IsActive = true;
            }

        }

        public Screen GetScreen<T> () where T : Screen {

            return _screens.FirstOrDefault( s => s.GetType() == typeof( T ) );
        }

        public void Dispose () {
            foreach ( Screen screen in _screens ) {
                screen.Dispose();
            }

            _screens.Clear();
        }
    }
}
