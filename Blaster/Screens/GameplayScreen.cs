﻿using Blaster.Entities;
using Blaster.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Screens {
    public sealed class GameplayScreen : Screen {

        public Level Level { get; private set; }

        public Player Player { get; private set; }

        private bool _waitingToContinue;

        public int Score { get; set; }

        public int WavesSurvived { get; private set; }

        public int CurrentLevel { get; private set; }

        public bool Paused { get; private set; }

        private int _scoreQueue;

        private KeyboardState _previousKeyboard;

        private Texture2D _pauseTexture;

        private Texture2D _scanlines;

        public GameplayScreen ( Game game ) : base(game) {

        }

        public void ChangeLevel ( int level ) {
            WavesSurvived = 0;
            Level = new Blaster.Level( Resources.GetTexture( "Level" + level + ".png" ) );
            Level.Scored += Level_Scored;
            Level.EntityDeath += Level_EntityDeath;
            Level.WaveEnded += Level_WaveEnded;
            Player = new Player( Level, new Vector2( 12 * 32, 12 * 32 ) );

            Level.Entities.Add( Player );

            CurrentLevel = level;
        }

        public override void Initialize () {
            ChangeLevel( 1 );
        }

        public override void LoadContent () {
            Player.LoadContent();

            _pauseTexture = new Texture2D( App.Instance.GraphicsDevice, 1, 1 );
            Color[] colorData = new Color[1];
            colorData[0] = Color.Black;
            _pauseTexture.SetData<Color>( colorData );

            _scanlines = Resources.GetTexture( "Grid.png" );

            base.LoadContent();
        }

       
        public override void Update ( Microsoft.Xna.Framework.GameTime gameTime ) {
            KeyboardState keyboard = Keyboard.GetState();

            if ( !Paused ) {

                if ( keyboard.IsKeyDown( Keys.Escape ) && !_previousKeyboard.IsKeyDown( Keys.Escape ) ) {
                    Paused = true;
                    _previousKeyboard = keyboard;
                    return;
                }

                if ( !_waitingToContinue ) {

                    if ( !Level.WaitingForNextWave ) {
                        Level.Update( gameTime );
                        //Player.Update( gameTime );

                        if ( _scoreQueue != 0 ) {
                            _scoreQueue -= 1;
                            Score += 1;
                        }
                    } else {
                        if ( keyboard.IsKeyDown( Keys.Enter ) && !_previousKeyboard.IsKeyDown( Keys.Enter ) ) {
                            Level.OnWaveStarted();
                            Level.WaitingForNextWave = false;
                            Level.WaveMax = (int)( Level.WaveMax * App.Fib );
                            Level.WaveCount = 0;
                            Level.WaveRemaining = Level.WaveMax;

                        }
                    }
                } else {

                    if ( keyboard.IsKeyDown( Keys.Enter ) && !_previousKeyboard.IsKeyDown( Keys.Enter ) ) {
                        _waitingToContinue = false;
                        Score = 0;
                        Level.WaitingForNextWave = false;
                        Level.WaveMax = 10;
                        Level.WaveCount = 0;
                        Level.WaveRemaining = Level.WaveMax;

                        Level.InitializeLevel();
                        Player.SetAlive();
                        Player.SetPosition( new Vector2( 12 * 32, 12 * 32 ) );
                        Level.Entities.Add( Player );

                    }
                }
            } else {

                if ( keyboard.IsKeyDown( Keys.Enter ) && !_previousKeyboard.IsKeyDown( Keys.Enter ) ) {
                    Paused = false;
                }

                if ( keyboard.IsKeyDown( Keys.Escape ) && !_previousKeyboard.IsKeyDown( Keys.Escape ) ) {
                    App.Instance.Stats.SetWavesSurvived( CurrentLevel, WavesSurvived );
                    App.Instance.ScreenManager.SetScreen( App.Instance.ScreenManager.GetScreen<LevelSelectionScreen>() );
                }
            }

            _previousKeyboard = keyboard;
        }

        public override void Draw ( Microsoft.Xna.Framework.GameTime gameTime, SpriteBatch spriteBatch ) {
            Level.Draw( gameTime, spriteBatch );
            //Player.Draw( gameTime, spriteBatch );

            string scoreString = String.Format( "SCORE: {0}", Score );
            int waveRemaining = Level.WaveMax - Level.WaveCount;
            Color renderColor = Color.Green;

            if ( waveRemaining >= 20 ) {
                renderColor = Color.Red;
            } else if ( waveRemaining < 20 && waveRemaining >= 15 ) {
                renderColor = Color.OrangeRed;
            } else if ( waveRemaining < 15 && waveRemaining >= 10 ) {
                renderColor = Color.Orange;
            } else if ( waveRemaining < 10 && waveRemaining >= 5 ) {
                renderColor = Color.Yellow;
            } else if ( waveRemaining < 5 && waveRemaining >= 0 ) {
                renderColor = Color.Green;
            }

            string waveLeftString = "REMAINING: " + (Level.WaveMax - Level.WaveCount).ToString();

            
            Vector2 scoreStringSize = App.Instance.UIFont.MeasureString( scoreString );
            Vector2 waveLeftStringSize = App.Instance.UIFont.MeasureString( waveLeftString );

            spriteBatch.DrawString( App.Instance.UIFont, String.Format( "SCORE: {0}", Score ), new Vector2( 6, 6 ), Color.Black );
            spriteBatch.DrawString( App.Instance.UIFont, String.Format("SCORE: {0}", Score), new Vector2(5, 5), Color.White );

            spriteBatch.DrawString( App.Instance.UIFont, waveLeftString, new Vector2( ( 800 - waveLeftStringSize.X ) - 6, 6 ), renderColor );
            spriteBatch.DrawString( App.Instance.UIFont, waveLeftString, new Vector2( ( 800 - waveLeftStringSize.X ) - 5, 5 ), Color.White );

            string ammoString = Player.Ammo > 0 ? "AMMO: " + Player.Ammo : "AMMO: INF.";

            spriteBatch.DrawString( App.Instance.StatsFont, ammoString, new Vector2( 6, 600 - 34 ), Color.Black );
            spriteBatch.DrawString( App.Instance.StatsFont, ammoString, new Vector2( 5, 600 - 35 ), Color.White );

            if ( Level.WaitingForNextWave ) {

                spriteBatch.Draw( _pauseTexture, new Rectangle( 0, 0, 800, 600 ), new Color( Color.Black, 100 ) );

                string message = "WAVE COMPLETED!";
                string messag2 = "PRESS ENTER";

                Vector2 messageSize = App.Instance.UIFont.MeasureString( message );
                Vector2 message2Size = App.Instance.UIFont.MeasureString( messag2 );

                spriteBatch.DrawString( App.Instance.UIFont, message, new Vector2(800 / 2 - messageSize.X / 2, 300), Color.White );
                spriteBatch.DrawString( App.Instance.UIFont, messag2, new Vector2( 800 / 2 - message2Size.X / 2, (message2Size.Y + 300) + 5 ), Color.White );
            }

            if ( Paused ) {
                spriteBatch.Draw( _pauseTexture, new Rectangle( 0, 0, 800, 600 ), new Color( Color.Black, 100 ) );

                string message = "PAUSED";
                string messag2 = "[ESC] EXIT, [ENTER] RESUME";

                Vector2 messageSize = App.Instance.UIFont.MeasureString( message );
                Vector2 message2Size = App.Instance.UIFont.MeasureString( messag2 );

                spriteBatch.DrawString( App.Instance.UIFont, message, new Vector2( 800 / 2 - messageSize.X / 2, 300 ), Color.White );
                spriteBatch.DrawString( App.Instance.UIFont, messag2, new Vector2( 800 / 2 - message2Size.X / 2, ( message2Size.Y + 300 ) + 5 ), Color.White );
            }

            spriteBatch.Draw( _scanlines, Vector2.Zero, new Color(Color.Black, 125) );
        }

        public override void Dispose () {
            Level.Dispose();

            base.Dispose();
        }

        #region Events
        void Level_EntityDeath ( object sender, EventArgs e ) {

            if ( e != null ) {
                if ( e is EntityDeathEventArgs ) {
                    EntityDeathEventArgs deathEventArgs = ( e as EntityDeathEventArgs );

                    if ( deathEventArgs.Entity != null ) {
                        if ( deathEventArgs.Entity is Player ) {
                            App.Instance.Stats.SetScore( Score );
                            App.Instance.Stats.SetWavesSurvived( CurrentLevel, WavesSurvived );
                            WavesSurvived = 0;
                            _waitingToContinue = true;
                        }
                    }
                }
            }
        }

        void Level_Scored ( object sender, EventArgs e ) {

            if ( e != null ) {
                if ( e is ScoreEventArgs ) {
                    _scoreQueue += ( e as ScoreEventArgs ).Score;
                }
            }
        }

        void Level_WaveEnded ( object sender, EventArgs e ) {
            WavesSurvived += 1;
        }

        #endregion

    }
}
