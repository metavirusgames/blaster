﻿using Blaster.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blaster.Screens {
    public sealed class LevelSelectionScreen : Screen {

        private List<Rectangle> _levelIcons;
        private Rectangle _lockIcon;
        private Rectangle _secretLevel;

        private int _currentPlayerSpot = 1;

        private Animation _animation;
        private bool _selectionLocked;
        private string _lockedString = "LOCKED";
        private Vector2 _lockedStringSize;
        private Texture2D _scanline;
        private string _currentSelectionStringQuote;
        private SoundEffect _changeSound;
        private SoundEffect _selectSound;

        public LevelSelectionScreen ( Game game )
            : base( game ) {
        }

        public override void Initialize () {
            _levelIcons = new List<Rectangle>();

            _levelIcons.Add( new Rectangle( 0, 0, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 32, 0, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 64, 0, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 96, 0, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 128, 0, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 160, 0, 32, 32 ) );

            _levelIcons.Add( new Rectangle( 0, 32, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 32, 32, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 64, 32, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 96, 32, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 128, 32, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 160, 32, 32, 32 ) );

            _levelIcons.Add( new Rectangle( 0, 64, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 32, 64, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 64, 64, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 96, 64, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 128, 64, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 160, 64, 32, 32 ) );

            _levelIcons.Add( new Rectangle( 0, 96, 32, 32 ) );
            _levelIcons.Add( new Rectangle( 32, 96, 32, 32 ) );

            _secretLevel = new Rectangle( 64, 96, 32, 32 ) ;
            _lockIcon = new Rectangle( 96, 96, 32, 32 );

            _animation = new Animation( Resources.GetTexture( "X4Animation.png" ), 100.0D );
            _animation.AddFrame( new Rectangle( 0, 32, 32, 32 ) );
            _animation.AddFrame( new Rectangle( 32, 32, 32, 32 ) );
            _animation.AddFrame( new Rectangle( 64, 32, 32, 32 ) );
            _animation.AddFrame( new Rectangle( 96, 32, 32, 32 ) );

            
        }

        public override void LoadContent () {

            _lockedStringSize = App.Instance.UIFont.MeasureString( _lockedString );
            _scanline = Resources.GetTexture( "Grid.png" );

            _changeSound = Resources.GetSoundEffect( "SelectLevel.wav" );
            _selectSound = Resources.GetSoundEffect( "Select.wav" );
            base.LoadContent();
        }

        private KeyboardState _previousKeyboardState;

        public override void Update ( Microsoft.Xna.Framework.GameTime gameTime ) {
            KeyboardState keyboard = Keyboard.GetState();

            if ( keyboard.IsKeyDown( Keys.Right ) && !_previousKeyboardState.IsKeyDown(Keys.Right)) {

                if ( _currentPlayerSpot + 1 > int.Parse( Resource.Levels ) ) {
                    _currentPlayerSpot = 1;
                } else {
                    _currentPlayerSpot += 1;
                }


                _changeSound.Play();
            }

            if ( keyboard.IsKeyDown( Keys.Left ) && !_previousKeyboardState.IsKeyDown( Keys.Left ) ) {
                if ( _currentPlayerSpot - 1 < 1 ) {
                    _currentPlayerSpot = int.Parse(Resource.Levels);
                } else {
                    _currentPlayerSpot -= 1;
                }

                _changeSound.Play();
            }

            _currentSelectionStringQuote = Resources.GetString( "Level" + _currentPlayerSpot + "Quote" );

            if ( !App.Instance.Stats.IsLevelUnlocked( _currentPlayerSpot ) ) {
                _selectionLocked = true;
            } else {
                _selectionLocked = false;
            }

            if ( keyboard.IsKeyDown( Keys.Enter ) && !_previousKeyboardState.IsKeyDown( Keys.Enter ) ) {
                if ( !_selectionLocked ) {
                    GameplayScreen gameplayScreen = (GameplayScreen)App.Instance.ScreenManager.GetScreen<GameplayScreen>();

                    if ( gameplayScreen != null ) {
                        gameplayScreen.Initialize();
                        gameplayScreen.ChangeLevel( _currentPlayerSpot );
                        gameplayScreen.LoadContent();
                        

                        App.Instance.ScreenManager.SetScreen( gameplayScreen );
                    } else {
                        gameplayScreen = new GameplayScreen( App.Instance );
                        gameplayScreen.Initialize();
                        gameplayScreen.ChangeLevel( _currentPlayerSpot );
                        gameplayScreen.LoadContent();
                        

                        App.Instance.ScreenManager.SetScreen( gameplayScreen );
                        _selectSound.Play();
                    }
                }
            }

            _animation.Update( gameTime );

            _previousKeyboardState = keyboard;
        }

        public override void Draw ( Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch ) {
            Vector2 selectionSize = new Vector2(10 * 64, 2 * 64) - new Vector2(32, 64);
            Vector2 renderOffset = new Vector2(800 / 2 - selectionSize.X / 2, 600 / 2 - selectionSize.Y);
            Vector2 quoteSize = App.Instance.UIFont.MeasureString( _currentSelectionStringQuote );

            int i = 1;

            spriteBatch.Draw( App.Instance.LevelSelectBG, Vector2.Zero, new Color(Color.LightBlue, 100) );
            spriteBatch.Draw( _scanline, Vector2.Zero, new Color(Color.White, 100) );

            for ( int y = 0; y < 1; y++ ) {
                for ( int x = 0; x < 10; x++ ) {

                    spriteBatch.Draw( App.Instance.LevelIcons, new Vector2( x * 64, y * 64 ) + renderOffset, _levelIcons[i - 1], Color.White );

                    if ( !App.Instance.Stats.IsLevelUnlocked( i ) ) {
                        spriteBatch.Draw( App.Instance.LevelIcons, new Vector2( x * 64, y * 64 ) + renderOffset, _lockIcon, Color.White );
                    }

                    if ( i == _currentPlayerSpot ) {
                        _animation.Draw( gameTime, (new Vector2( x * 64, y * 64 ) + renderOffset) - new Vector2(2, 2), spriteBatch, Microsoft.Xna.Framework.Graphics.SpriteEffects.None );
                    }

                    i += 1;
                }
            }

            if ( _selectionLocked ) {
                spriteBatch.DrawString( App.Instance.UIFont, _lockedString, new Vector2( 800 / 2 - _lockedStringSize.X / 2, 100 ) + new Vector2(1, 1), Color.Black );
                spriteBatch.DrawString( App.Instance.UIFont, _lockedString, new Vector2( 800 / 2 - _lockedStringSize.X / 2, 100 ), Color.White );

            } else {
                spriteBatch.DrawString( App.Instance.UIFont, _currentSelectionStringQuote, new Vector2( 800 / 2 - quoteSize.X / 2, 100 ) + new Vector2( 1, 1 ), Color.Black );
                spriteBatch.DrawString( App.Instance.UIFont, _currentSelectionStringQuote, new Vector2( 800 / 2 - quoteSize.X / 2, 100 ), Color.White );
            }
        }
    }
}
