﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Screens {
    public abstract class Screen : IDisposable {

        protected Game Game { get; private set; }

        public Screen ( Game game ) {
            Game = game;
        }

        public bool IsActive { get; set; }

        public bool ContentLoaded { get; private set; }

        public abstract void Initialize ();

        public virtual void LoadContent () {
            ContentLoaded = true;
        }

        public abstract void Update ( GameTime gameTime );

        public abstract void Draw ( GameTime gameTime, SpriteBatch spriteBatch );

        public virtual void Dispose () {
            
        }
    }
}
