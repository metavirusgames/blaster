﻿using Blaster.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blaster.Events {
    public sealed class EntityDeathEventArgs : EventArgs {

        public CharacterEntity Entity { get; private set; }

        public EntityDeathEventArgs ( CharacterEntity entity ) {
            Entity = entity;
        }
    }
}
