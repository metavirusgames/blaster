﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Blaster {
    public sealed class Resources {

        //public static Stream GetFile ( string filename ) {
        //    string[] files = Assembly.GetExecutingAssembly().GetManifestResourceNames();

        //    Stream result = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Blaster.Resources." + filename ); //Resource.ResourceManager.GetStream(String.Format("{0}.{1}.{2}","Blaster", "Resources", filename) );

        //    return result;
        //}

        public static Texture2D GetTexture ( string filename ) {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Blaster.Resources." + filename ); //Resource.ResourceManager.GetStream(String.Format("{0}.{1}.{2}","Blaster", "Resources", filename) );

            Texture2D texture = Texture2D.FromStream( App.Instance.GraphicsDevice, stream );
            stream.Close();
            stream.Dispose();

            return texture;
        }

        public static SpriteFont GetSpriteFont ( string filename ) {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Blaster.Resources." + filename ); //Resource.ResourceManager.GetStream(String.Format("{0}.{1}.{2}","Blaster", "Resources", filename) );
            string outputFile = Guid.NewGuid().ToString() + ".xnb";

            Stream output = new FileStream(outputFile, FileMode.Create);
            stream.CopyTo( output );
            StreamWriter writer = new StreamWriter(output );
            writer.Flush();
            writer.Close();

            SpriteFont font = App.Instance.Content.Load<SpriteFont>( outputFile.Replace(".xnb", "" ));

            stream.Close();
            stream.Dispose();

            File.Delete( outputFile );

            return font;
        }

        public static SoundEffect GetSoundEffect ( string filename ) {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream( "Blaster.Resources." + filename );
            SoundEffect sound = SoundEffect.FromStream( stream );

            stream.Close();
            stream.Dispose();

            return sound;
        }

        public static string GetString ( string key ) {

            return Resource.ResourceManager.GetString( key );
        }
    }
}
